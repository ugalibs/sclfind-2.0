#!/bin/bash
cd /app/apache2/htdocs
bundle install --path vendor/bundle/
RAILS_ENV=docker bundle exec rake db:migrate
sudo apachectl -D FOREGROUND
