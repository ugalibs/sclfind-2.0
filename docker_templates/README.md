# Using the SCLFind local docker env

You can use these files to run a local development environment with docker-compose (version 1.28 or newer).

To get started:
  
0)  Get the master key file from another developer or from the CI variable storage.  
1)  Copy docker_templates/env_example to .env so that docker-compose can find it.  If you are at the root of the repository on *nix like machines this would be `cp docker_templates/env_example .env`  
2)  Make any customizations to the .env file that you want to.  None are necessary out of the box.
3)  From the root of the repository, run `scripts/setup_dev_env.sh`.  This will take a minute or two to run.
4)  When the script finishes if everything needed was present in the .env file, your environment should be running.  You can use commands like `docker-compose ps` to check the status of the containers.  Note that there will be an index running for the first minute or two after setup. 

