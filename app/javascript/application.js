// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "controllers";
import "@hotwired/turbo-rails";
import bootstrap from "bootstrap";
import githubAutoCompleteElement from "@github/auto-complete-element";
import Blacklight from "blacklight";
import "arclight";

import { createApp } from "vue";

// ---------------------------------------------------------------------
// 1) Advanced Search Modal
// ---------------------------------------------------------------------
function createAdvancedSearchModal() {
    const advancedModalContainer = document.createElement('div');
    advancedModalContainer.id = 'vue-advanced-search-container';
    document.body.appendChild(advancedModalContainer);

    const app = createApp({
        data() {
            return {
                query: "",
                selectedField: "all_fields",
                fieldOptions: [
                    { label: "All Fields", value: "all_fields" },
                    { label: "Keyword",    value: "keyword" },
                    { label: "Name",       value: "name" },
                    { label: "Place",      value: "place" },
                    { label: "Subject",    value: "subject" },
                    { label: "Title",      value: "title" },
                ],
                selectedLocation: "",
                locationOptions: [
                    { label: "No Location Limit", value: "" },
                    { label: "Richard B. Russell Library for Political Research and Studies", value: "Richard B. Russell Library for Political Research and Studies" },
                    { label: "Hargrett Library", value: "Hargrett Library" },
                    { label: "Hargrett Map Collection", value: "Hargrett Map Collection" },
                    { label: "University of Georgia Archives", value: "University of Georgia Archives" },
                    { label: "Historic Clothing and Textile Collection", value: "Historic Clothing and Textile Collection" },
                ],

                // boolean search
                criteria: [
                    { operator: "", query: "" },
                ],
                booleanOps: ["AND", "OR", "NOT"],

                hasOnlineContent: false,
            };
        },
        template: `
      <div class="modal-overlay" 
           @click.self="closeModal" 
           role="dialog" 
           aria-modal="true" 
           aria-labelledby="advanced-search-modal-title">
        <div class="modal-content">
          <div class="modal-header">
            <h1 id="advanced-search-modal-title">Advanced Search</h1>
            <button type="button" class="btn-close" @click="closeModal" aria-label="Close"></button>
          </div>

          <div class="modal-body advanced-search-scrollable">
            <form @submit.prevent="executeSearch">
              
              <!-- Search Field Dropdown -->
              <div class="mb-3">
                <label for="fieldSelect" class="form-label">Search In:</label>
                <select 
                  id="fieldSelect" 
                  class="form-select" 
                  v-model="selectedField"
                >
                  <option 
                    v-for="(option, index) in fieldOptions" 
                    :value="option.value" 
                    :key="index"
                  >
                    {{ option.label }}
                  </option>
                </select>
              </div>

              <!-- Physical Location -->
              <div class="mb-3">
                <label for="locationSelect" class="form-label">Physical Location:</label>
                <select
                  id="locationSelect"
                  class="form-select"
                  v-model="selectedLocation"
                >
                  <option
                    v-for="(loc, idx) in locationOptions"
                    :key="idx"
                    :value="loc.value"
                  >
                    {{ loc.label }}
                  </option>
                </select>
              </div>

              <!-- online content -->
              <div class="form-check mb-3">
                <input 
                  type="checkbox" 
                  class="form-check-input" 
                  id="onlineContentCheck" 
                  v-model="hasOnlineContent"
                />
                <label class="form-check-label" for="onlineContentCheck">
                  Has online content
                </label>
              </div>

              <!-- Adding rows -->
              <div class="mb-3" v-for="(row, index) in criteria" :key="index">
                <div class="mb-2 d-flex align-items-end" v-if="index > 0">
                  <div class="me-3">
                    <label class="form-label">Operator:</label>
                    <select v-model="row.operator" class="form-select">
                      <option 
                        v-for="op in booleanOps" 
                        :key="op" 
                        :value="op"
                      >
                        {{ op }}
                      </option>
                    </select>
                  </div>
                  <button 
                    type="button" 
                    class="btn btn-outline-danger"
                    @click="removeRow(index)"
                  >
                    Remove
                  </button>
                </div>

                <label :for="'queryInput' + index" class="form-label">
                  {{ index === 0 ? "First Term:" : "Term:" }}
                </label>
                <input
                  :id="'queryInput' + index"
                  type="text"
                  class="form-control"
                  v-model="row.query"
                  placeholder="Enter search term..."
                />
              </div>

              <button 
                type="button" 
                class="btn btn-outline-secondary mt-2"
                @click="addRow"
              >
                + Add Another Term
              </button>

              <div class="mt-4">
                <button class="btn btn-primary" type="submit">
                  Search
                </button>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button class="btn btn-secondary" @click="closeModal">Close</button>
          </div>
        </div>
      </div>
    `,
        methods: {
            closeModal() {
                app.unmount();
                advancedModalContainer.remove();
            },
            addRow() {
                this.criteria.push({ operator: "AND", query: "" });
            },
            removeRow(index) {
                if (this.criteria.length > 1) {
                    this.criteria.splice(index, 1);
                }
            },
            executeSearch() {
                // Combine all boolean terms into a single q
                let combinedQuery = "";
                this.criteria.forEach((row, index) => {
                    const trimmed = row.query.trim();
                    if (!trimmed) return; // skip empty

                    if (index === 0) {
                        // first row
                        combinedQuery += trimmed;
                    } else {
                        combinedQuery += ` ${row.operator} ${trimmed}`;
                    }
                });

                const urlParams = new URLSearchParams();
                urlParams.set("group", "true");
                urlParams.set("search_field", this.selectedField);
                urlParams.set("q", combinedQuery);

                if (this.selectedLocation) {
                    urlParams.append("f[repository][]", this.selectedLocation);
                }

                if (this.hasOnlineContent) {
                    urlParams.append("f[access][]", "online");
                }

                window.location.href = `/catalog?${urlParams.toString()}`;
            }
        }
    });

    const componentInstance = app.mount(advancedModalContainer);
    advancedModalContainer.__vue_app__ = componentInstance;
}

function createCartModal() {
    console.log('Creating new cart modal');
    const modalContainer = document.createElement('div');
    modalContainer.id = 'vue-modal-container';
    document.body.appendChild(modalContainer);

    const app = createApp({
        data() {
            return {
                items: [],
                notes: '' // For the notes textarea
            };
        },
        template: `
      <div class="modal-overlay" @click.self="closeModal" role="dialog" aria-modal="true" aria-labelledby="cart-modal-title">
        <div class="modal-content">
          <div class="modal-header">
            <h1 id="cart-modal-title">Cart</h1>
            <button type="button" class="btn-close" @click="closeModal" aria-label="Close"></button>
          </div>
          <div class="contact-info">
            Please contact Special Collections for material retrieval or photo duplication services.
            Call <a href="tel:706-542-7123">706-542-7123</a> or email
            <a href="mailto:sclib@uga.edu">sclib@uga.edu</a>.
          </div>
          <div class="modal-body advanced-search-scrollable">
            <div v-if="items.length > 0">
              <h3>Items in your cart ({{ items.length }}):</h3>
              <ul>
                <li v-for="(item, index) in items" :key="index">
                  <strong>Title:</strong> {{ item.title }} <br>
                  <strong>Physical Location:</strong> {{ item.physloc }} <br>
                  <strong>Collection:</strong> {{ item.collection }} <br>
                  <strong>Container ID:</strong> {{ item.parent }} <br>
                  <strong>Box ID:</strong> {{ item.containers }} <br>
                  <strong>Library Location:</strong> {{ item.repository }} <br>
            
                  <button class="btn btn-sm btn-danger" @click="removeItem(index)">Remove</button>
                </li>
              </ul>
            </div>
            <div v-else class="empty-cart">Your cart is empty.</div>
            <div class="notes-section">
              <label for="cart-notes">Notes for your request:</label>
              <textarea id="cart-notes" v-model="notes" placeholder="Add any notes here..."></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <div class="cart-message">
              Items will stay in your cart until you remove them or place an order.<br/>
              Note that clicking "Place Order" will clear your cart, even if you do not complete the request with Aeon.
            </div>
            <div class="cart-buttons">
              <button class="btn btn-primary" @click="submitCart" :disabled="items.length === 0">Place Order</button>
              <button class="btn btn-danger" @click="clearAll">Clear All</button>
            </div>
          </div>
        </div>
      </div>
    `,
        methods: {
            closeModal() {
                console.log('Close cart modal button clicked');
                this.saveCart();
                console.log('Cart saved');

                app.unmount();
                console.log('Cart Vue app unmounted');

                modalContainer.remove();
                console.log('Cart modal container removed from DOM');

                modalContainer.__vue_app__ = null;
                console.log('Cart Vue app reference cleared');
            },
            updateCartCountDisplay() {
                const cartTotalElem = document.getElementById('cart-total');
                if (cartTotalElem) {
                    cartTotalElem.textContent = this.items.length;
                }
            },
            addItem(title, physloc, collection, parent, containers, repository) {
                const keepFirstSegment = (value) => {
                    if (!value) return "";
                    return value.split(",")[0].trim();
                };

                const cleanedParent = keepFirstSegment(parent);
                const cleanedContainers = keepFirstSegment(containers);

                const exists = this.items.some(item =>
                    item.title === title &&
                    item.physloc === physloc &&
                    item.collection === collection &&
                    item.parent === cleanedParent &&
                    item.containers === cleanedContainers &&
                    item.repository === repository
                );

                if (!exists) {
                    this.items.push({
                        title,
                        physloc,
                        collection,
                        parent: cleanedParent,
                        containers: cleanedContainers,
                        repository
                    });
                    console.log(`Item added: ${title} at ${physloc}`);
                    this.saveCart();
                } else {
                    console.log("Item already in cart:", title, physloc);
                }
                this.updateCartCountDisplay();
            },
            removeItem(index) {
                this.items.splice(index, 1);
                console.log(`Item removed at index: ${index}`);
                this.saveCart();
                this.updateCartCountDisplay();
            },
            clearAll() {
                this.items = [];
                this.notes = '';
                this.saveCart();
                console.log('All items cleared from the cart');
                this.updateCartCountDisplay();
            },
            submitCart() {
                console.log("Submitting cart to Aeon...", this.items, "Notes:", this.notes);

                const form = document.createElement("form");
                form.style.display = "none";
                form.method = "POST";
                form.action = "https://uga.aeon.atlas-sys.com/logon"; // Aeon endpoint

                const baseFields = [
                    { name: "AeonForm", value: "EADRequest" },
                    { name: "RequestType", value: "Loan" },
                    { name: "Site", value: "SCLFind" },
                    { name: "DocumentType", value: "" },
                    { name: "SubmitButton", value: "Submit Request" }
                ];
                baseFields.forEach(field => {
                    const input = document.createElement("input");
                    input.type = "hidden";
                    input.name = field.name;
                    input.value = field.value;
                    form.appendChild(input);
                });

                const notesInput = document.createElement("input");
                notesInput.type = "hidden";
                notesInput.name = "Notes";
                notesInput.value = this.notes || "";
                form.appendChild(notesInput);

                this.items.forEach((item, i) => {

                    const requestInput = document.createElement("input");
                    requestInput.type = "hidden";
                    requestInput.name = "Request";
                    requestInput.value = i;
                    form.appendChild(requestInput);

                    const locInput = document.createElement("input");
                    locInput.type = "hidden";
                    locInput.name = `Location_${i}`;
                    locInput.value = item.repository || "";
                    form.appendChild(locInput);

                    const itemTitleInput = document.createElement("input");
                    itemTitleInput.type = "hidden";
                    itemTitleInput.name = `ItemTitle_${i}`;
                    itemTitleInput.value = item.collection || "";
                    form.appendChild(itemTitleInput);

                    const subTitleInput = document.createElement("input");
                    subTitleInput.type = "hidden";
                    subTitleInput.name = `ItemSubTitle_${i}`;
                    subTitleInput.value = item.title || "";
                    form.appendChild(subTitleInput);

                    const numberInput = document.createElement("input");
                    numberInput.type = "hidden";
                    numberInput.name = `ItemNumber_${i}`;
                    numberInput.value = item.physloc || "";
                    form.appendChild(numberInput);

                    const refInput = document.createElement("input");
                    refInput.type = "hidden";
                    refInput.name = `ReferenceNumber_${i}`;
                    refInput.value = item.parent || "";
                    form.appendChild(refInput);

                    const subLocInput = document.createElement("input");
                    subLocInput.type = "hidden";
                    subLocInput.name = `SubLocation_${i}`;
                    subLocInput.value = item.containers || "";
                    form.appendChild(subLocInput);
                });

                document.body.appendChild(form);

                form.submit();

                this.items = [];
                this.notes = "";
                this.saveCart();
                this.closeModal();
            },
            saveCart() {
                const cartData = {
                    items: this.items,
                    notes: this.notes
                };
                localStorage.setItem('cartItems', JSON.stringify(cartData));
                console.log('Cart saved to localStorage');
            },
            loadCart() {
                const savedCart = localStorage.getItem('cartItems');
                if (savedCart) {
                    const cartData = JSON.parse(savedCart);
                    this.items = cartData.items || [];
                    this.notes = cartData.notes || '';
                    console.log('Cart loaded from localStorage');
                }
            }
        },
        created() {
            this.loadCart();
            this.updateCartCountDisplay();
        }
    });

    const componentInstance = app.mount(modalContainer);
    modalContainer.__vue_app__ = componentInstance;

    console.log('Cart modal created and Vue app mounted');
}

function updateCartCountFromStorage() {
    const savedCart = localStorage.getItem('cartItems');
    const cartTotalElem = document.getElementById('cart-total');

    if (savedCart && cartTotalElem) {
        const cartData = JSON.parse(savedCart);
        cartTotalElem.textContent = (cartData.items || []).length;
    }
}

function handleCartClick() {
    console.log('Cart link clicked');

    const existingModal = document.getElementById('vue-modal-container');
    if (existingModal && existingModal.__vue_app__) {
        console.log('Existing cart modal found');
        return;
    }

    createCartModal();
}

window.addToCart = function(title, physloc, collection, parent, containers, repository) {
    console.log('Adding to cart:', title, physloc, collection, parent, containers, repository);

    const existingModal = document.getElementById('vue-modal-container');
    if (existingModal && existingModal.__vue_app__) {
        existingModal.__vue_app__.addItem(title, physloc, collection, parent, containers, repository);
    } else {
        createCartModal();
        setTimeout(() => {
            const newModal = document.getElementById('vue-modal-container');
            if (newModal && newModal.__vue_app__) {
                newModal.__vue_app__.addItem(title, physloc, collection, parent, containers, repository);
            }
        }, 0);
    }
};

function attachEventListeners() {
    // cart
    const cartLink = document.getElementById('cart-link');
    if (cartLink) {
        cartLink.addEventListener('click', (event) => {
            event.preventDefault();
            handleCartClick();
        });
        console.log('Cart link event listener attached');
    } else {
        console.log('Cart link not found');
    }

    // adv
    const advancedSearchButton = document.getElementById('advanced-search-button');
    if (advancedSearchButton) {
        advancedSearchButton.addEventListener('click', (event) => {
            event.preventDefault();

            const existingModal = document.getElementById('vue-advanced-search-container');
            if (existingModal && existingModal.__vue_app__) {
                console.log('Advanced Search modal already exists.');
                return;
            }

            createAdvancedSearchModal();
        });
        console.log('Advanced search event listener attached');
    } else {
        console.log('Advanced search button not found');
    }
}

document.addEventListener('click', (event) => {
    const button = event.target.closest('.request-carton-button');
    if (!button) return;

    event.preventDefault();
    const title = button.dataset.title;
    const physloc = button.dataset.physloc;

    const collection = button.dataset.collection;
    const parent = button.dataset.parent;
    const containers = button.dataset.containers;
    const repository = button.dataset.repository;

    console.log('Delegated event for request-carton-button:', title, physloc);

    window.addToCart(title, physloc, collection, parent, containers, repository);
});

document.addEventListener('DOMContentLoaded', () => {
    const proxy = document.getElementById('online_content_proxy');
    const realCheckbox = document.getElementById('online_content');
    if (proxy && realCheckbox) {
        if (realCheckbox.checked) {
            proxy.checked = true;
        }
        proxy.addEventListener('change', () => {
            realCheckbox.checked = proxy.checked;
        });
    }

    document.querySelectorAll('table.table.table-striped').forEach(table => {
        table.setAttribute('role', 'presentation');
    });
});

document.addEventListener('turbo:frame-load', (event) => {
    if (event.target.id.includes('al-hierarchy-')) {
        attachEventListeners();
    }
});

document.addEventListener('DOMContentLoaded', () => {
    console.log('DOMContentLoaded fired');
    updateCartCountFromStorage();
    attachEventListeners(); // Attach both CART + ADV SEARCH
});

document.addEventListener('turbo:load', () => {
    console.log('turbo:load fired');
    updateCartCountFromStorage();
    attachEventListeners(); // Re-attach if Turbo visits
});

document.addEventListener('turbo:frame-loading', (event) => {
    if (event.target.id.includes('al-hierarchy-')) {
        event.target.classList.add('my-hierarchy-loading');
    }
});

document.addEventListener('turbo:frame-load', (event) => {
    if (event.target.id.includes('al-hierarchy-')) {
        event.target.classList.remove('my-hierarchy-loading');
    }
});
