import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="toc"
export default class extends Controller {
  placeholderTitle = '{{Placeholder Title}}';
  placeholderId = '_placeholder_id_';
  placeholderCount = '_placeholder_child_count_';

  templates = {};

  getTemplate(template_name) {
    let template = this.templates[template_name];
    if (template) return template;
    template = document.querySelector(`[data-toc-template=${template_name}]`)?.innerHTML;
    this.templates[template_name] = template;
    return template;
  }

  async getWithCache(src) {
    const cache = await window.caches?.open('toc');
    if (!cache) return await fetch(src);

    const hit = await cache.match(src);
    if (hit) return hit;
    const response = await fetch(src);
    await cache.put(src, response.clone());
    return response;
  }

  renderTOCNode(el, result, expandIndex, childNestPath, level) {
    const ul = document.createElement('ul');
    ul.setAttribute('class', 'documents')
    const docs = result.items || [];
    let collapseAboveFrom, collapseAboveTo, collapseBelow;
    if (docs.length > 20 && level > 0) {
      if (isNaN(expandIndex)) {
        collapseBelow = 10;
      } else {
        if (expandIndex > 11) {
          collapseAboveFrom = 3;
          collapseAboveTo = expandIndex - 3;
        }
        if (docs.length - 1 - expandIndex > 11) {
          collapseBelow = expandIndex + 3;
        }
      }
    }
    let html = '';
    docs.forEach((doc, i) => {
      const template = this.getTemplate((doc.child_count > 0) ? 'branch' : 'leaf');
      if (i === collapseAboveFrom || i === collapseBelow) {
        html += '<div class="collapse">';
      }
      html += template.replaceAll(this.placeholderTitle, doc.label)
          .replaceAll(this.placeholderId, doc.id)
          .replaceAll(this.placeholderCount, doc.child_count);
      if (i === collapseAboveTo || (collapseBelow && i === docs.length - 1)) {
        html += '</div><button class="btn btn-secondary btn-sm" data-expand>Expand</button>'
      }
    });
    ul.innerHTML = html;
    ul.querySelectorAll('[data-expand]').forEach(button => {
      button.addEventListener('click', function () {
        button.previousElementSibling.classList.add('show');
        button.parentElement.removeChild(button);
      });
    });
    const items = ul.querySelectorAll('li');
    for(let i=0; i<items.length; i++) {
      const li= items[i];
      const expandedOrSelected = i === expandIndex;
      const expanded = childNestPath && expandedOrSelected;
      const selected = !childNestPath && expandedOrSelected;
      const frame = li.querySelector('turbo-frame');
      if (selected) {
        li.classList.add('al-hierarchy-highlight');
      }
      if (!frame) {
        continue;
      }
      const childSrc = frame.getAttribute('src');
      if (childSrc) {
        let jsonSrc = childSrc + '&format=json';
        if (expanded) jsonSrc += `&nest_path=${childNestPath}`;
        frame.setAttribute('data-json-src', jsonSrc);
        let initatedLoad = false;
        if (expanded) {
          initatedLoad = true;
          this.loadTOCNode(frame, level + 1);
          const toggle = frame.closest('li').querySelector('a.al-toggle-view-children');
          toggle.classList.remove('collapsed');
          toggle.setAttribute('aria-expanded', 'true');
          frame.closest('.al-collection-context-collapsible').classList.add('show');
        }
        frame.addEventListener('turbo:before-fetch-request', e => {
          e.preventDefault();
          e.stopPropagation();
          if (initatedLoad) return;
          initatedLoad = true;
          this.loadTOCNode(frame, level + 1);
        });
      }
    }
    el.innerHTML = '';
    el.appendChild(ul);
    el.removeAttribute('loading');

    //This will give screen readers more descriptive text when on the collapsed/expanded links.
    const liItems = ul.querySelectorAll('li');
    liItems.forEach((li, i) => {
      const doc = docs[i];
      if (doc) {
        const toggle = li.querySelector('a.al-toggle-view-children');
        if (toggle) {
          toggle.setAttribute('aria-label', `Toggle contents of ${doc.label}`);
        }
      }
    });
  }

  loadTOCNode(el, level) {
    level ||= 0;
    const src = el.getAttribute('data-json-src');
    const searchParams = new URLSearchParams(src.split('?')[1] || '');
    const nestPath = (searchParams.get('nest_path') || '').split('/')
        .map(comp => comp.split('#')[1])
        .filter(comp => comp !== undefined);
    const expandIndex = parseInt(nestPath[0]); // may be NaN
    const childNestPath = nestPath.slice(1).map(comp => `%2Fcomponents%23${comp}`).join('');
    searchParams.delete('nest_path');
    const srcWithoutNestPath = src.split('?')[0] + '?' + searchParams.toString();
    this.getWithCache(srcWithoutNestPath).then(response => {
      response.json().then(result => {
        this.renderTOCNode(el, result, expandIndex, childNestPath, level);
      });
    });
  }

  loadTOCNodes() {
    document.querySelectorAll('turbo-frame[data-json-src][loading=lazy]').forEach(
        node => this.loadTOCNode(node)
    );
  }

  connect() {
    this.loadTOCNodes();
  }
}