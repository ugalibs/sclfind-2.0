# frozen_string_literal: true

class SidebarWithJsonTocComponent < Arclight::SidebarComponent

  PLACEHOLDER_TITLE = '{{Placeholder Title}}'
  PLACEHOLDER_ID = '_placeholder_id_'
  PLACEHOLDER_CHILD_COUNT = '_placeholder_child_count_'

  class PlaceholderNumber
    def initialize(str_value, value = 1)
      @str_value = str_value
      @value = value
    end

    delegate_missing_to :@value

    def to_s
      @str_value
    end
  end

  def placeholder_leaf
    {
      'normalized_title_ssm' => PLACEHOLDER_TITLE,
      '_nest_path_' => 'inner_nest_path',
      'id' => PLACEHOLDER_ID
    }
  end

  def placeholder_branch
    placeholder_leaf.merge 'child_component_count_isi' => PlaceholderNumber.new(PLACEHOLDER_CHILD_COUNT)
  end

  def template_for_doc(doc, template_name, nest_path)
    content_tag('script', type: 'text/html', data: {'toc-template' => template_name}) do
      presenter = Blacklight::DocumentPresenter.new(SolrDocument.new(doc), view_context)
      component =  Arclight::DocumentCollectionHierarchyComponent.new(document: presenter,
                                                                      nest_path: nest_path,
                                                                      blacklight_config: view_context.blacklight_config)
      render component
    end
  end

  def turbo_frame_tag(*args, **options)
    content_tag :div, data: {controller: 'toc'} do
      super(
        *args,
        **options.without(:src).merge(data: {'json-src' => options[:src]})
      ) +
        template_for_doc(placeholder_leaf, 'leaf', 'outer_nest_path') +
        template_for_doc(placeholder_branch, 'branch', 'outer_nest_path')
    end
  end

  def hierarchy_solr_document_path(*args, **options)
    super(*args, **options.merge(format: 'json'))
  end


end
