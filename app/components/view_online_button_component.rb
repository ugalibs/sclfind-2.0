# frozen_string_literal: true

class ViewOnlineButtonComponent < Arclight::OnlineStatusIndicatorComponent

  def render?
    @document.online_content? && @document.online_content_href
  end

  def call
    link_to "View Online", @document.online_content_href, class: "btn btn-primary", target:"_blank"
  end
end

