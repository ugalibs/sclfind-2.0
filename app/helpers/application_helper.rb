module ApplicationHelper
  include Blacklight::LocalePicker::LocaleHelper

  def additional_locale_routing_scopes
    [blacklight, arclight_engine]
  end

  def homepage?
    controller_name == 'home' && action_name == 'index'
  end
end
