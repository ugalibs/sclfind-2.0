module SearchHelper
  def custom_within_collection_options(collection_name)
    options_for_select(
      [
        [t('arclight.within_collection_dropdown.all_collections'), ''],
        [t('arclight.within_collection_dropdown.this_collection'), collection_name || 'none-selected']
      ],
      selected: '',  #Sets default to all collections
      disabled: 'none-selected'
    )
  end
end
