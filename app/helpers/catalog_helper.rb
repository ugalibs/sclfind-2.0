module CatalogHelper
  include Blacklight::CatalogHelperBehavior

  def render_request_container(args)
    document = args[:document] || @document
    render 'catalog/request_item', document: document
  end

  def render_view_digital_version(args)
    document = args[:document] || @document
    if document.online_content?
      render 'catalog/view_digital_version', document: document
    end
  end

  def render_request_a_scan(_args)
    render 'catalog/request_a_scan'
  end

  def render_how_to_request(_args)
    render 'catalog/how_to_request'
  end

  def render_how_to_visit(_args)
    render 'catalog/how_to_visit'
  end

  def render_contact(_args)
    render 'catalog/contact'
  end
end
