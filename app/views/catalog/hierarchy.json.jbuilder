json.items do
  @counter = 0
  json.array! @response.documents do |doc|
    json.id doc.id
    json.label doc.normalized_title
    json.url url_for(doc)
    json.link(link_to_document doc, counter: @counter)
    json.child_count doc['child_component_count_isi']
    @counter += 1
  end
end