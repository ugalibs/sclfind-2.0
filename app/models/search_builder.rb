# frozen_string_literal: true
class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior
  include Arclight::SearchBehavior

  self.default_processor_chain += [:skip_collection_subquery_for_tree_view]
  def skip_collection_subquery_for_tree_view(solr_parameters)
    return unless blacklight_params['action'] == 'hierarchy' && blacklight_params['format'] == 'json'
    solr_parameters[:fl]='*'
    solr_parameters['hl'] = false
  end
end
