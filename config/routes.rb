Rails.application.routes.draw do
  mount Blacklight::Engine => '/'
  mount Arclight::Engine => '/'

  root to: "arclight/repositories#index"
  concern :searchable, Blacklight::Routes::Searchable.new
  concern :exportable, Blacklight::Routes::Exportable.new
  concern :hierarchy, Arclight::Routes::Hierarchy.new

  # Custom route for Solr documents with special characters in ID
  get 'catalog/:id', to: 'catalog#show', constraints: { id: /[^\/]+/ }, as: :custom_catalog

  resource :catalog, only: [:index], as: 'catalog', path: '/catalog', controller: 'catalog' do
    concerns :searchable
  end

  devise_for :users

  resources :solr_documents, only: [:show], path: '/catalog', controller: 'catalog' do
    concerns :hierarchy
    concerns :exportable
  end

  #Overwrite Bookmark routes
  match '/bookmarks', to: redirect('/'), via: :all
  match '/bookmarks/*path', to: redirect('/'), via: :all

end
