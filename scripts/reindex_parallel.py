#!/usr/bin/python3

import subprocess
import os

# Define repositories
sclfind_repos = ["gua-hctc","gua-hmap","gua-russ","gua-ua","gua-harg"]

# Create ENVs for repositories (required for the reindex command
repo_envs = {}
for repo in sclfind_repos:
    repo_envs[repo] = os.environ.copy()
    repo_envs[repo]["DIR"] = "/app/sclfind-eads/"+repo
    repo_envs[repo]["REPOSITORY_ID"] = repo

# Destroy existing index
clear=subprocess.Popen(['bundle','exec','rails','arclight:destroy_index_docs'],cwd='/app/apache2/htdocs',env=repo_envs[sclfind_repos[0]])
clear.wait()


# Reindex in parallel
for repo in sclfind_repos:
    p=subprocess.Popen(['bundle','exec','rails','arclight:index_dir'],cwd='/app/apache2/htdocs',env=repo_envs[repo])
p.wait()

# Reindex complete
