#!/bin/bash

bundle exec rails arclight:destroy_index_docs

echo "Reindex in progress"

DIR=$PWD/docker_templates/dev_eads/gua-harg REPOSITORY_ID=gua-harg bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-hctc REPOSITORY_ID=gua-hctc bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-hmap REPOSITORY_ID=gua-hmap bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-russ REPOSITORY_ID=gua-russ bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-ua REPOSITORY_ID=gua-ua bundle exec rails arclight:index_dir

echo "Reindex complete"

