#!/bin/bash

source .env

echo "Building containers with defaults"
docker-compose build
echo "Starting containers"
docker-compose up -d
sleep 3
echo "Preparing solr core"
docker-compose exec solr /opt/solr/bin/solr create_core -c blacklight-core -d /opt/solr/server/solr/arclight
echo "Initializing database and running migrations"
bundle install && RAILS_ENV=development bundle exec rake db:migrate
echo "Indexing files in /app/sclfind-eads"
DIR=$PWD/docker_templates/dev_eads/gua-harg REPOSITORY_ID=gua-harg bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-hctc REPOSITORY_ID=gua-hctc bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-hmap REPOSITORY_ID=gua-hmap bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-russ REPOSITORY_ID=gua-russ bundle exec rails arclight:index_dir
DIR=$PWD/docker_templates/dev_eads/gua-ua REPOSITORY_ID=gua-ua bundle exec rails arclight:index_dir
