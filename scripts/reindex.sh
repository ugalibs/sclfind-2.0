#!/bin/bash
cd /app/apache2/htdocs
bundle exec rails arclight:destroy_index_docs

echo "Reindex in progress"


DIR=/app/sclfind-eads/gua-hctc REPOSITORY_ID=gua-hctc bundle exec rails arclight:index_dir 

DIR=/app/sclfind-eads/gua-hmap REPOSITORY_ID=gua-hmap bundle exec rails arclight:index_dir 

DIR=/app/sclfind-eads/gua-russ REPOSITORY_ID=gua-russ bundle exec rails arclight:index_dir 

DIR=/app/sclfind-eads/gua-ua REPOSITORY_ID=gua-ua bundle exec rails arclight:index_dir 

DIR=/app/sclfind-eads/gua-harg REPOSITORY_ID=gua-harg bundle exec rails arclight:index_dir 

echo "Reindex complete"

