#!/bin/bash
cd /app/apache2/htdocs
bundle install --path vendor/bundle/
bundle exec rake db:migrate
bundle exec rake assets:precompile
passenger-config restart-app .
