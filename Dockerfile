FROM ubuntu:20.04

ENV TZ="Americas/New_York"
ENV DEBIAN_FRONTEND=noninteractive

#build dependencies
RUN apt-get -qq update && apt-get -qq install libreadline-dev libssl-dev libpcre3-dev libexpat1-dev nfs-common build-essential bison zlib1g-dev libxss1 libappindicator1 libindicator7 libcurl4-openssl-dev git libmysqlclient-dev libsqlite3-dev libyaml-dev libffi-dev nodejs sudo tzdata wget curl

#install ruby
ARG RUBY_VERSION_MAJOR=3.2
ARG RUBY_VERSION_MINOR=2
ARG RUBY_CONFIG_LINE="--prefix=/app/ruby --disable-install-doc --enable-load-relative --enable-shared"

#install ruby
RUN wget https://cache.ruby-lang.org/pub/ruby/$RUBY_VERSION_MAJOR/ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR.tar.gz && tar xzf ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR.tar.gz && cd ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR && ./configure $RUBY_CONFIG_LINE && make -j$(nproc) && make install && cd .. && rm -rf ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR*

#install psql client
ARG POSTGRESQL_VERSION=14.11
ARG POSTGRESQL_CONFIG_LINE="--with-openssl"
RUN wget https://ftp.postgresql.org/pub/source/v$POSTGRESQL_VERSION/postgresql-$POSTGRESQL_VERSION.tar.gz && tar xzf postgresql-$POSTGRESQL_VERSION.tar.gz && cd postgresql-$POSTGRESQL_VERSION && ./configure $POSTGRESQL_CONFIG_LINE && make -C src/bin install && make -C src/include install && make -C src/interfaces install && cd .. && rm -rf postgresql-$POSTGRESQL_VERSION*

#install apache
ARG APACHE_VERSION=2.4.62

RUN apt -qq update && apt-get -qq install curl wget gnupg && apt-get -qq install wget vim libreadline-dev libssl-dev libpcre3-dev libexpat1-dev build-essential bison zlib1g-dev libxss1 libappindicator1 libindicator7 sudo tzdata unzip less

RUN wget https://ai.galib.uga.edu/files/httpd-$APACHE_VERSION-w-apr.tar.gz && tar xzf httpd-$APACHE_VERSION-w-apr.tar.gz && cd httpd-$APACHE_VERSION && ./configure  '--prefix=/app/apache2' '--with-apxs2=/app/apache2/bin/apxs' '--with-mysqli' '--with-pear' '--with-xsl' '--with-pspell' '--enable-ssl' '--with-gettext' '--with-gd' '--enable-mbstring' '--with-mcrypt' '--enable-soap' '--enable-sockets' '--with-libdir=/lib/i386-linux-gnu' '--with-jpeg-dir=/usr' '--with-png-dir=/usr' '--with-curl' '--with-pdo-mysql' '--enable-so' '--with-included-apr' && make -j$(nproc) && make install && cd ..&& rm -rf httpd-$APACHE_VERSION*

ENV PATH /app/apache2/bin:$PATH

#fix path for apache
ENV PATH /app/ruby/bin:/usr/local/pgsql/bin:$PATH
RUN sed -i "s/\/snap\/bin/\/snap\/bin:\/app\/ruby\/bin:\/usr\/local\/pgsql\/bin\/app\/apache2\/bin/" /etc/sudoers

#configure passenger
RUN gem update --system && gem install -f passenger bundler && passenger-install-apache2-module --snippet | tee /app/apache2/conf/passenger.conf && echo "Include conf/passenger.conf" | tee -a /app/apache2/conf/httpd.conf && passenger-install-apache2-module -a --languages ruby

#Add local user, gitlab-runner in this case for continuity w/pipelined servers
#If you have permission issues set the LOCAL_UID to match your local user and rebuild the container.
ARG LOCAL_UID=1000
RUN adduser --uid $LOCAL_UID --gecos "GitLab Runner" --disabled-password gitlab-runner

#Copy necessary files into container
COPY docker_templates/gitlab-runner /etc/sudoers.d/
COPY docker_templates/startup.sh /
COPY docker_templates/apache/sclfind.conf /app/apache2/conf/

ARG RAILS_APP_ENV=development
ARG SERVERNAME=localhost
ARG SOLR_URL=http://solr:8983/solr/blacklight-core

RUN echo "Include conf/sclfind.conf" | tee -a /app/apache2/conf/httpd.conf && sed -i "s/RAILS_APP_ENV/$RAILS_APP_ENV/" /app/apache2/conf/sclfind.conf &&  sed -i "s/RAILS_APP_ENV/$RAILS_APP_ENV/" /startup.sh && sed -i "s/SERVERNAME/$SERVERNAME/" /app/apache2/conf/sclfind.conf && echo "export SOLR_URL=$SOLR_URL" >> /app/apache2/bin/envvars

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - && apt-get update && apt-get install -y nodejs && npm install -y --global yarn

USER gitlab-runner

WORKDIR /app/apache2/htdocs

CMD ["/startup.sh"]
